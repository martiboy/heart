

var scene = new THREE.Scene();

var camera = new THREE.PerspectiveCamera(75, window.innerWidth/window.innerHeight, 0.1, 1000);
camera.position.z = 150;

var renderer = new THREE.WebGLRenderer({antialias:true});
renderer.setClearColor("#87CEEB");
renderer.setSize(window.innerWidth, window.innerHeight);

document.body.appendChild(renderer.domElement);

window.addEventListener("resize", () =>{
    renderer.setSize(window.innerWidth, window.innerHeight);
    camera.aspect = window.innerWidth/window.innerHeight;
    
    camera.updateProjectionMatrix();
});

var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();
var spin = false;
var x = -25, y = -50;
var shape = new THREE.Shape();
shape.moveTo(x + 25, y + 25);
shape.bezierCurveTo(x + 25, y + 25, x + 20, y, x, y);
shape.bezierCurveTo(x - 30, y, x - 30, y + 35, x - 30, y + 35);
shape.bezierCurveTo(x - 30, y + 55, x - 10, y + 77, x + 25, y + 95);
shape.bezierCurveTo(x + 60, y + 77, x + 80, y + 55, x + 80, y + 35);
shape.bezierCurveTo(x + 80, y + 35, x + 80, y, x + 50, y);
shape.bezierCurveTo(x + 35, y, x + 25, y + 25, x + 25, y + 25);
  
const extrudeSettings = {
    steps: 20,
    depth: 20,
    bevelEnabled: true,
    bevelThickness: 10,
    bevelSize: 10,
    bevelSegments: 20,
  };
var geometry = new THREE.ExtrudeGeometry(shape, extrudeSettings);
var material = new THREE.MeshLambertMaterial({color: 0xFFB1A0});
var mesh = new THREE.Mesh(geometry, material);
var rotationSpeed = 0.0;
mesh.rotation.set(0, 0, 531);
scene.add(mesh);


var ambientLight = new THREE.AmbientLight(0xFFB1A0);
var spotLight = new THREE.SpotLight(0xffffff);
spotLight.position.set( 500, 1000, 100 );
spotLight.castShadow = true;
spotLight.shadow.mapSize.width = window.innerWidth;
spotLight.shadow.mapSize.height = window.innerHeight;
spotLight.shadow.camera.near = 500;
spotLight.shadow.camera.far = 4000;
spotLight.shadow.camera.fov = 30;

scene.add(ambientLight, spotLight);

var render = function() {
    requestAnimationFrame(render);
    mesh.rotation.y += rotationSpeed 
    renderer.render(scene, camera);
}

function rotateHeart(){
    requestAnimationFrame(rotateHeart);
    rotationSpeed += 0.03
    if(rotationSpeed > 0.03){
        rotationSpeed = 0.03
    }
}


function onMouseClick(event){
    event.preventDefault();

    mouse.x = 2 * (event.clientX / window.innerWidth) - 1;
    mouse.y = 1 - 2 * ( event.clientY / window.innerHeight );

    raycaster.setFromCamera(mouse, camera);

    $( ".quotes" ).toggle( "slow", "linear", function() {
    });
        rotateHeart();
}

window.addEventListener('click', onMouseClick);


render();


